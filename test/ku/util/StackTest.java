package ku.util;
import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

/**
 * Test stack of type0 and type1.
 * @author Jidapar Jettananurak
 *
 */
public class StackTest {

	Stack stack = StackFactory.makeStack( 5 );
	
	/** "Before" method is run before each test. */
	@Before
	public void runBeforeTest(){
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack( 4 );
	}
	
	/**	"After" method is run after tests are tested. */
	@After
	public void runAfterTest(){
		stack = null;
	}
	
	/**	Test capacity of stack */
	@Test
	public void testCapacity(){
		stack = StackFactory.makeStack( 1 );
		assertEquals( stack.capacity() , 1 );
	}
	
	/**	Test size of stack */
	@Test
	public void testIsSize(){
		stack.push( 1 );
		assertEquals( 1 , stack.size() );
	}
	
	/**	Test stack is empty */
	@Test
	public void newStackIsEmpty(){
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0 , stack.size() );
	}
	
	/**	Test stack is full */
	@Test
	public void testStackIsFull(){
		stack.push( 1 );
		stack.push( 2 );
		stack.push( 3 );
		stack.push( 4 );
		assertTrue( stack.isFull() );
	}

	/**	Stack is can pop */
	@Test 
	public void testPop(){
		stack.push( 1 );
		assertEquals( 1 , stack.pop() );
	}
	
	/** pop() should throw an exception if stack is empty */
	@Test( expected = java.util.EmptyStackException.class )
	public void testPopEmptyStack() {
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
	}
	
	/**	Stack is can peek */
	@Test
	public void testPeek(){
		stack.push( 1 );
		assertEquals( 1 , stack.peek() );
	}
	
	/**	Stack is can push */
	@Test
	public void testPush(){
		assertFalse( stack.isFull() );
		stack.push(new Object());
	}
	
	/**	Test stack is IllegalArgument */
	@Test( expected = IllegalArgumentException.class )
	public void testPushInvalidArgument(){
		stack.push(null);
	}
	
	/**	Test stack is IllegalState */
	@Test( expected = IllegalStateException.class )
	public void testPushIllegalState(){
		stack.push( 1 );
		stack.push( 2 );
		stack.push( 3 );
		stack.push( 4 );
		stack.push( 5 );
	}
	
	

}
